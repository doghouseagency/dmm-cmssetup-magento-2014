# CMS Setup

Creates CMS pages or blocks easily in your data install or upgrade scripts. Also
puts dummy content in there (from http://hipsum.co/).

Do this in your data install/upgrade script:

	$installer = $this;

	$installer->startSetup();

	//You don't have to do this next step if $installer is already an instance of
	//Doghouse_CmsSetup_Model_Setup (if you specify this in your etc/config.xml)
	$cmsinstaller = new Doghouse_CmsSetup_Model_Setup($this->_resourceName);

	//Creates blocks
	$cmsinstaller->createBlock('contact-details', 'Contact Details');
	$cmsinstaller->createBlock('home-offers', 'News & Offers', '<p>Hellow orld</p>');

	//Creates pages
	$cmsinstaller->createPage('shipping', 'Shipping');
	$cmsinstaller->createPage('returns', 'Returns');
	$cmsinstaller->createPage('payment', 'Payment', '<p>Hellow orld</p>');

	$installer->endSetup();

The first parameter is the URL (in case of a page), or identifier (in the case of a block).

The second parameter is the title.

If you don't specify the third parameter - which is the content - it grabs three
paragraphs from HipsterIpsum through it's API (http://hipsterjesus.com/).

What's that you say? You just want to use this module to get hipsterIpsum? Here you go:

	$setup = new Doghouse_CmsSetup_Model_Setup('default_setup');

	$text = $setup->getHipsum(3); //3 paragraphs

If the Hipsum API isn't available, or if 5 seconds pass by without cURL getting a response,
it will just use boring Lorem Ipsum instead :[ (hardcoded).

Thanks yo.