<?php

class Doghouse_CmsSetup_Model_Setup extends Mage_Core_Model_Resource_Setup
{

    public $defaultParagraphs = 2;

    /**
     * Hipsum generator using hipsterjesus API!
     * http://hipsterjesus.com/
     * @param  integer $paragraphs number of paragraphs
     * @return string
     */
    public function getHipsum($paragraphs = null)
    {

        if (!$paragraphs) {
            $paragraphs = $this->defaultParagraphs;
        }

        //http://stackoverflow.com/questions/10236166/does-file-get-contents-have-a-timeout-setting
        $ctx = stream_context_create(array('http'=>
            array(
                'timeout' => 5,
            )
        ));

        $url = sprintf("http://hipsterjesus.com/api/?paras=%s&html=true&type=hipster-latin", $paragraphs);

        @$content = file_get_contents($url, false, $ctx);

        if (strlen($content) === 0) {
            //Regular lorem ipsum :(
            return 'Lorem ipsum dolor sit amet, consectetur adipiscing elit.';
        }

        $content = json_decode($content);
        return $content->text;

    }

    /**
     * Creates a CMS static block. Doesn't do anything if the block with specified
     * identifier already exists.
     * @return Mage_Cms_Model_Block
     */
    public function createBlock($identifier, $title = "Hello World!", $html = null)
    {

        if (!$html) {
            $html = $this->getHipsum();
        }

        $data = array(
            'title'         => $title,
            'identifier'    => $identifier,
            'content'       => $html,
            'is_active'     => 1,
            'stores'        => 0
        );

        try {

            $block = Mage::getModel('cms/block')->load($identifier, 'identifier');

            if (!$block->getid()) {
                $block->setData($data)->save();
            }

        } catch (Exception $e) {
            Mage::logException($e);
        }

        return $block;

    }

    /**
     * Creates a CMS page. Doesn't do anything if a page with that url key already exists
     * @return Mage_Cms_Model_Page
     */
    public function createPage($urlkey, $title = "Hello World!", $html = null)
    {

        if (is_null($html)) {
            $html = $this->getHipsum();
        }

        $data = array(
            'title'             => $title,
            'identifier'        => $urlkey,
            'root_template'     => 'one_column',
            'meta_description'  => '',
            'stores'            => array(0), //available for all store views
            'content'           => $html,
            'content_heading'   => $title,
            'is_active'         => 1
        );

        try {

            $page = Mage::getModel('cms/page')->load($urlkey, 'identifier');

            if (!$page->getid()) {
                $page->setData($data)->save();
            }

        } catch (Exception $e) {
            Mage::logException($e);
        }

        return $page;
    }

}
